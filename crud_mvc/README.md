# Tutorial CRUD em MVC

* Autor: Marcelo Akira Inuzuka <marceloakira@ufg.br>
* Licença: cc-by-sa


## 1. Introdução

Neste tutorial, faremos a implementação de um exemplo de utilização do padrão [MVC](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller) em uma aplicação [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete), que é o acrônimo para Create, Retrieve, Update e Delete, que são operações básicas para a persistência em uma base de dados. O código-fonte, bem como o projeto bluej estão neste mesmo repositório do [Gitlab](https://gitlab.com/ap2-ec-2020-2/tutorials/-/tree/master/crud_mvc). 

## 2. Requisitos de software

Para realizar os passos do tutorial é necessário a instalação dos seguintes softwares:

* O [BlueJ 4.X](https://bluej.org/) é um software educativo utilizado para ensino de programação Java, com foco na Orientação a Objetos, com suporte integrado e interativo com um [diagrama de classes](https://en.wikipedia.org/wiki/Class_diagram). Além do BlueJ, é necessário também instalar a [extensão Getter Setter](), bastando baixar o [arquivo .jar](https://www.bluej.org/extensions/files/GetterSetterExtension.jar) e copiá-lo na pasta &lt;bluej-dir&gt;/lib/extensions, note que esta extensão é **compatível somente com a versão 4.X do BlueJ**. 

* [Apache Netbeans](https://netbeans.apache.org/) é uma IDE (Ambiente Integrado de Desenvolvimento) para Java que será utilizado para construir a interface gráfica. Sugere-se instalar a versão mais nova ([12.3](https://netbeans.apache.org/download/nb123/nb123.html), por exemplo), para a sua plataforma ([Windows](https://www.apache.org/dyn/closer.cgi/netbeans/netbeans/12.3/Apache-NetBeans-12.3-bin-windows-x64.exe) ou [Linux](https://www.apache.org/dyn/closer.cgi/netbeans/netbeans/12.3/Apache-NetBeans-12.3-bin-linux-x64.sh), por exemplo).    

* Driver JDBC para acesso ao banco de dados [SQLite](https://www.sqlite.org/index.html): o arquivo .jar pode ser o mais atual, e pode ser baixado do [repositório do site do projeto Maven](https://search.maven.org/artifact/org.xerial/sqlite-jdbc), por exemplo, a versão [3.34.0](https://repo1.maven.org/maven2/org/xerial/sqlite-jdbc/3.34.0/sqlite-jdbc-3.34.0.jar). O arquivo jar tem que ser instalado no BlueJ na pasta [&lt;bluej-dir&gt;/lib/userlib](https://www.bluej.org/faq.html#faq_How_do_I_use_custom_class_libraries__JARs__). 

* Instalar as [bibliotecas *.jar](https://ormlite.com/releases/) do [OrmLite](https://ormlite.com/) 5.3 ([core](https://ormlite.com/releases/5.3/ormlite-core-5.3.jar) e [jdbc](https://ormlite.com/releases/5.3/ormlite-jdbc-5.3.jar)) no BlueJ: o OrmLite é uma biblioteca que permite  arquivos *.jar podem ser baixados do [site oficial do ormlite](https://ormlite.com/releases/). Da mesma forma, deve ser baixado e copiado para a mesma pasta userlib anterior.

## 3. Baixar o código fonte do tutorial

 Faça o download do código-fonte deste tutorial ([crud_mvc](https://gitlab.com/ap2-ec-2020-2/tutorials/-/tree/master/crud_mvc)) do gitlab. Uma sugestão é baixar a pasta crud_mvc, com a opção e "[Download this directory](https://gitlab.com/ap2-ec-2020-2/tutorials/-/archive/master/tutorials-master.zip?path=crud_mvc)". Uma vez baixado, você pode abrir o projeto no BlueJ (pasta bluej) e importar o projeto no Netbeans (opção File > Import Project > From Zip). As telas com os dois projetos ficarão assim:

 ![BlueJ](bluej.png)

 ![Netbeans](netbeans.png)

 ## 4. Conceitos sobre MVC e Padrão Repository

Leia sobre os conceitos sobre MVC nos [slides](https://web.stanford.edu/class/cs75n/1_MVC.pdf) do [Curso de CS 75N da Universidade de Stanford](https://web.stanford.edu/class/cs75n/). O padrão MVC estabelece várias regras a serem seguidas na comunicação entre os componentes da aplicação. Preste atenção nos princípios básicos, principalmente no slide 8:

![MVC](mvc.png)

Os componentes controladores servem como intermediadores na comunicação entre os componentes da visão e do modelo, que não conversam diretamente. A ideia deste padrão de projeto arquitetural é dividir a responsabilidade dos componentes da aplicação:
  * **Modelo**: componentes responsáveis por conter os dados da aplicação, que compõe o [estado da aplicação](https://en.wikipedia.org/wiki/State_(computer_science)) como um todo, podendo persistir ou não em uma base de dados. Não possui interface visual e na programação em Java, são apelidados de [Plain Old Java Objects](https://en.wikipedia.org/wiki/Plain_old_Java_object), por serem simples, não possuirem lógica de interação, limitando-se a [métodos setters e getters](https://dzone.com/articles/java-getter-and-setter-basics-common-mistakes-and). Definem o **que** (what) sua aplicação é, mas não **como** é apresentada.
  * **Visão**: componentes responsáveis por apresentar visualmente os modelos para o usuário, bem como interagir com o usuário por meio de uma interface, por exemplo: Gráfica ([Graphical User Interface](https://en.wikipedia.org/wiki/Graphical_user_interface)), por janelas; Web ([Web User Interface](https://en.wikipedia.org/wiki/Web_application)), pelo Navegador Web; ou até por linha de comando (CLI) ou interface móvel em celular. No padrão MVC, a visão é **dominada pelos controladores**. 
  * **Controlador**: componentes responsáveis pela lógica da aplicação, ou seja, controlam **como** (how) os dados dos modelos são apresentados ao usuário. Recebem notificações por meios de eventos produzidos por componentes de visão ou modelo, respectivamente, por exemplo: evento de clique em um botão ou a alteração externa dos dados de um modelo por meio de um sensor (neste caso, os controladores são notificados e o [padrão de projeto Observer](https://pt.wikipedia.org/wiki/Observer) é geralmente utilizado)

Outro ponto relevante neste conteúdo são os slides (29 a 49), que tratam da comunicação entre os "MVCs", que pode ser caótica caso não seja sistematizada de forma correta:

![mvcs](mvcs.png)

A solução para este problema é a utilização de um controlador de controladores:

![controllers](controllers.png)

Neste tutorial, conforme pode ser visto na tela do BlueJ, o controlador dos controladores vai ser a classe *AppControler* que comunica com o *StudentRepositoryController* para embutir a classe *StudenteRepositoryView* dentro do componente *AppView*.

Outro conteúdo que é importante ler é o artigo "[Learn to make a MVC application with Swing and Java 8](https://ssaurel.medium.com/learn-to-make-a-mvc-application-with-swing-and-java-8-3cd24cf7cb10)" que demonstra na prática como implementar uma aplicação MVC simples. O componente modelo é um simples POJO e a tela da visão pode ser construída facilmente em uma IDE do tipo Netbeans. O foco de atenção tem que ser no método *initController* da classe *Controller*:

```java
 public void initController() {
  view.getFirstnameSaveButton().addActionListener(e -> saveFirstname());
  view.getLastnameSaveButton().addActionListener(e -> saveLastname());
  view.getHello().addActionListener(e -> sayHello());
  view.getBye().addActionListener(e -> sayBye());
 }
 private void saveFirstname() {
  ///...
 }
 private void saveLastname() {
  ///...
 }
 private void sayHello() {
  ///...
 }
 private void sayBye() {
  ///...
 }
 ```

Note que neste método é que se associa os eventos que ocorrem nos componentes visuais (botões *FirstnameSave* e *LastnameSave* e as caixas de texto *Hello* e *Bye*) da visão (*view*), respectivamente com os métodos *saveFirstname*, *saveLastName*, *sayHello* e *sayBye*. Ou seja, para cada ação em um componente visual, há uma lógica a ser executada por um método específico no *Controller*. Cada componente da *view* é acessado por um *getter* e o método *addActionListener* atribui um método local, quando ocorrer um evento (e). A sintaxe (e -&gt; &lt;método&gt;) foi criado na versão 8 do Java é é chamado de [funções Lambda](https://dzone.com/articles/java-8-lambda-functions-usage-examples), que utiliza o operador *arrow* (-&gt;) para associar um argumento de entrada (no nosso caso, um evento *e*) a uma função de tratamento do evento. 

E para finalizar a leitura de conceitos, vale a pena conhecer o padrão *Repository*, fazendo a leitura do artigo "[DAO vs Repository Patterns](https://www.baeldung.com/java-dao-vs-repository)". Este artigo, compara dois tipos de padrões para acesso a objetos armazenados em um banco de dados: o padrão *Dao* é adequado para lidar com persistência de objetos individuais e o padrão *Repository* é mais adequado a coleções de objetos, podendo ser mistos (vários tipos de objetos) ou não. Neste tutorial, adotaremos o padrão *Repository*:

```java
public interface UserRepository {
    User get(Long id);
    void add(User user);
    void update(User user);
    void remove(User user);
}
public class UserRepositoryImpl implements UserRepository {
    private UserDaoImpl userDaoImpl;
    
    @Override
    public User get(Long id) {
        User user = userDaoImpl.read(id);
        return user;
    }

    @Override
    public void add(User user) {
        userDaoImpl.create(user);
    }

    // ...
}
```
O padrão *Repository* é utilizado em aplicações CRUD que gerenciam a persistência de objetos que já suportam o padrão *Dao*, conforme pode ser observado no relacionamento entre as classes *UserRepositoryImpl* e *UserDaoImpl*. Neste tutorial, utilizaremos as classe *StudentRepository* e *Student*, que seguirão o padrão *Repository*. Há [dezenas de padrões de projeto](https://en.wikipedia.org/wiki/Software_design_pattern), caso queira saber mais, caso queira saber mais recomendo dois livros: "[Patterns of Enterprise Application Architecture](https://martinfowler.com/books/eaa.html)" de Martin Fowler ou "[Design Patterns: Elements of Reusable Object-Oriented Software](https://en.wikipedia.org/wiki/Design_Paterns)" de Erich Gamma, Richard Helm
Ralph Johnson e John Vlissides.

## 5. Compilando e testando

Com o código-fonte baixado e com dependências instaladas, compile e teste a aplicação CRUD deste tutorial. 

![testando](testando1.gif)

Note que a classe *AppController* possui um método *main* que pode ser chamado para executar a aplicação:

```java
    public static void main(String[] args) {
        AppModel model = new AppModel();
        AppView view = new AppView();
        AppController ac = new AppController(model, view);
        view.setVisible(true);
    }
```

Basicamente, o método *main* instancia um *model* e uma *view*, e passa-os como argumento na instanciação de um *controller*, finalmente tornando a *view* visível com o método *setVisible(true)*. 

Outros testes na interface podem ser feitos e pode-se constatar que as funcionalidades de *create* e *search* foram implementadas, mas as funcionalidades de *update*, *delete* e navegação entre os registros não foram implementados, o que será feito como exercício.

## 6. Iniciando um novo projeto do 'zero'

Agora que já temos uma noção onde podemos alcançar, vamos começar um projeto do 'zero' para conhecer passo a passo cada parte de um projeto de uma aplicação CRUD que segue o padrão MVC.

Os seguintes passos serão tomados a seguir:
* **Model**: definir um componente *model* básico *Student*, que utiliza a camada de persistência para salvar os objetos. Além da classe POJO *Student*, a classe *StudentRepository* será criada para lidar com um repositório de objetos *Student*.
* **View**: construir a interface gráfica da aplicação. Para esta tarefa, é conveniente utiliza uma IDE, como o Netbeans, para lidar com os componentes visuais.
* **Controller**: desenvolver a lógica de controle da visão com o modelo.
* **Aplicação**: conforme conceito visto anterior, criar um 'MVC App' que controla outros 'MVCs'. 

### 6.1. Model: entidades e persistência 

A parte mais básica é o acesso a uma base de dados. Poderíamos adotar um banco de dados sofisticado, como o PostgreSQL ou Oracle, mas o foco não é esse, então podemos adotar algo bem mais simples como o [Sqlite](https://www.sqlite.org/index.html). Além de ser leve, o SQLite também não necessita instalação, o próprio driver (JDBC) cria e gerencia um arquivo de banco de dados.

Outra tecnologia adotada neste tutorial é o [Ormlite](https://ormlite.com/), que provê a persistência de objetos em banco de dados relacional (baseado em tabelas), ou seja, é também conhecido como ORM ([Object Relational Mapping](https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping) ou [Mapeamento Objeto-Relacional](https://pt.wikipedia.org/wiki/Mapeamento_objeto-relacional)). A linguagem utilizada para interação com banco de dados relacional é o [SQL](https://pt.wikipedia.org/wiki/SQL) (Structered Query Language), mas com o ORMLite, não será necessário utilizá-la, no lugar, será utilizado métodos como *create*, *query*, *update* e *delete*. Assim, a aplicação se tornará mais simples e legível.

#### 6.1.1. Database: acesso ao banco de dados

O primeiro passo da construção da aplicação é criar uma classe responsável por gerenciar a conexão com o banco de dados. Crie a seguinte classe no BlueJ:

```java
import java.sql.*;
import com.j256.ormlite.jdbc.JdbcConnectionSource;

public class Database
{
   private String databaseName = null;
   private JdbcConnectionSource connection = null;
   
   public Database(String databaseName) {
       this.databaseName = databaseName;
   }    
   
   public JdbcConnectionSource getConnection() throws SQLException {
      if ( databaseName == null ) {
          throw new SQLException("database name is null");
      }
      if ( connection == null ) {
          try {
              connection = new JdbcConnectionSource("jdbc:sqlite:"+databaseName);             
            } catch ( Exception e ) {
                System.err.println( e.getClass().getName() + ": " + e.getMessage() );
                System.exit(0);
            }
            System.out.println("Opened database successfully");
      }
      return connection;
   }
   
   public void close() {
       if ( connection != null ) {
           try {
               connection.close();
               this.connection = null;
           } catch (java.io.IOException e) {
               System.err.println(e);
           }
       }
   }
}
```

Note no exemplo acima que foi implementado três métodos:
* Um método constutor que recebe o caminho do arquivo de base de dados, a propriedade *databaseName*;
* Um método *getConnection()* que inicializa a propriedade *connection* com um objeto [JdbcConnectionSource](https://ormlite.com/javadoc/ormlite-jdbc/com/j256/ormlite/jdbc/JdbcConnectionSource.html). Os detalhes de como criar uma conexão com o SQLite é descrito na [sdocumentação do ORMLite](https://ormlite.com/javadoc/ormlite-core/doc-files/ormlite.html#Connection-Source) e o [formato da string de conexão (jdbc:sqlite:&lt;caminho para arquivo&gt;)](https://www.sqlitetutorial.net/sqlite-java/sqlite-jdbc-driver/) utilizadas para se definir uma URL de conexão é documentado no driver JDBC/SQLite.
* Um método de *close()* para fechar a base de dados.

Para testar interativamente a classe, utilize o BlueJ:

![database](database.gif)

#### 6.1.2. Student: entidade estudante

O próximo passo é criar uma classe que representa uma entidade do mundo real, um estudante, por exemplo. Uma classe entidade (entity class) é basicamente, um POJO, com suas propriedades acessíveis via *getters* e *setters*, podendo então ser persistidas em um banco de dados via um ORM (ORMLite, por exemplo). Crie a seguinte classe Estudante no BlueJ:

```java
import java.util.Date;
import java.text.SimpleDateFormat;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;

@DatabaseTable(tableName = "student")
public class Student
{   
    @DatabaseField(generatedId = true)
    private int id;
    
    @DatabaseField
    private String fullName;
    
    @DatabaseField
    public int registration;
    
    @DatabaseField(dataType=DataType.DATE)
    public Date birthday;    
    
    public String printBirthday() {
        SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
        return dateFor.format(birthday);
    }
}
```

A forma mais simples de se criar classes entidade é usando o recurso de [anotações java](https://en.wikipedia.org/wiki/Java_annotation), que consiste em fornecer metainformação sobre um membro da classe (exemplo, classes, propriedades e métodos). Cada linguagem de programação adota uma sintaxe de anotação, em java temos:

```java
@<Classe de Anotação>(param1="valor1", param2="valor2", ...)
<Classe ou membro>
```

Por exemplo:

```java
@DatabaseTable(tableName = "student")
public class Student
...
```
A classe de anotação é [DatabaseTable](https://ormlite.com/javadoc/ormlite-core/com/j256/ormlite/table/DatabaseTable.html), que anota a classe *Student*, informando que a tabela a ser gravada tem nome *student*, conforme o parâmetro *tableName*. Na classe entidade *Student*, também foi utilizado a classe de anotação [DatabaseField](https://ormlite.com/javadoc/ormlite-core/com/j256/ormlite/field/DatabaseField.html) que informou que a propriedade *id* da classe é autogerado ([*generatedId*](https://ormlite.com/javadoc/ormlite-core/com/j256/ormlite/field/DatabaseField.html#generatedId--)) e o tipo de dados [DataType.DATE](https://ormlite.com/javadoc/ormlite-core/com/j256/ormlite/field/DataType.html#DATE) atribuído a propriedade *birthDay*. Outras propriedades como *fullName* e *registration* são respectivamente do tipo *int* e *String* e sua anotação não precisa de associar a um [DataType](https://ormlite.com/javadoc/ormlite-core/com/j256/ormlite/field/DataType.html) específico, basta anotá-los com [*DatabaseField*](https://ormlite.com/javadoc/ormlite-core/com/j256/ormlite/field/DatabaseField.html).

#### 6.1.3. StudentRepository: classe padrão Repository

Para lidar com entidades, pode-se adotar o [padrão Repository](https://martinfowler.com/eaaCatalog/repository.html), que possui várias vantagens como [permitir trocar o banco de dados sem afetar o sistema como um todo](https://medium.com/@renicius.pagotto/entendendo-o-repository-pattern-fcdd0c36b63b) e também facilita [testes da camada de persistência, com independência das camadas superiores](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/infrastructure-persistence-layer-design). Nessa classe vamos implementar operações básicas CRUD, manter um cache dos objetos carregados (loaded) das consultas realizadas na base de dados e também o estado do repositório (deletando ou criando objetos, por exemplo). Crie a classe *StudentRepository* no BlueJ:

```java
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

public class StudentRepository
{
    private static Database database;
    private static Dao<Student, Integer> dao;
    private List<Student> loadedStudents;
    private Student loadedStudent; 
    
    public StudentRepository(Database database) {
        StudentRepository.setDatabase(database);
        loadedStudents = new ArrayList<Student>();
    }
    
    public static void setDatabase(Database database) {
        StudentRepository.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Student.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Student.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    }
    
    public Student create(Student student) {
        int nrows = 0;
        try {
            nrows = dao.create(student);
            if ( nrows == 0 )
                throw new SQLException("Error: object not saved");
            this.loadedStudent = student;
            loadedStudents.add(student);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return student;
    }    

    public void update(Student student) {
      // TODO
    }

    public void delete(Student student) {
      // TODO
    }
    
    public Student loadFromId(int id) {
        try {
            this.loadedStudent = dao.queryForId(id);
            if (this.loadedStudent != null)
                this.loadedStudents.add(this.loadedStudent);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedStudent;
    }    
    
    public List<Student> loadAll() {
        try {
            this.loadedStudents =  dao.queryForAll();
            if (this.loadedStudents.size() != 0)
                this.loadedStudent = this.loadedStudents.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedStudents;
    }

    // getters and setters ommited...        
}
```

Através do BlueJ, compile, gere os *getters* e *setters* das classes. Com isso, você pode realizar alguns testes:

![repository](repository.gif)

O construtor de *StudentRepository* recebe um objeto *database* e o atribui a uma variável de classe *database*. Poderíamos utilizar uma variável de instância para referenciar a base de dados, mas por escolha de projeto, considerou-se que era mais conveniente ter apenas uma única instância *database* associada a classe *StudentRepository*, evita-se assim, indesejáveis conexões concorrentes a uma mesma base de dados. Outro ponto importante no construtor, é a utilização da classe DaoManager do Ormlite para criar um [Objeto de Acesso a Dados](https://en.wikipedia.org/wiki/Data_access_object), uma instância da [classe Dao do Ormlite](https://ormlite.com/javadoc/ormlite-core/com/j256/ormlite/dao/Dao.html) e o método [*createTableIfNotExists* da classe TableUtils](https://ormlite.com/javadoc/ormlite-core/com/j256/ormlite/table/TableUtils.html) que cria uma tabela *student* na base de dados caso ela não exista.

Note nos testes acima, que um objeto *StudentRepository* persiste objetos *Student* por meio do método *create()*. Quando se cria um objeto *Student* ele é um objeto transiente, ou seja, existe somente em memória. Quando se passa um objeto transiente como argumento para o método *create*, ele chama uma instância [Dao do Ormlite](https://ormlite.com/javadoc/ormlite-core/com/j256/ormlite/dao/Dao.html) para chamar um método homônimo *create* que persiste o objeto no banco de dados SQLite.

Outro ponto importante a ser notado é o método *loadAll*, que carrega objetos do banco de dados para a memória, invocando o método homônimo do objeto *dao* e retorna uma lista de todos estudantes carregados em memória, a propriedade *loadedStudents*. De forma análoga, o método *loadFromId* faz a carga de um estudante da base de dados a partir de um *id* informado, atribuindo-se o objeto carregado a propriedade *loadedStudent*.  

Caso queira visualizar os dados na forma de tabelas, pode-se utilizar uma ferramenta como o [DB Browser for SQLite](https://sqlitebrowser.org/), que possui versões para [Linux](https://sqlitebrowser.org/dl/#linux:~:text=Linux,-DB) e [Windows](https://github.com/sqlitebrowser/sqlitebrowser/releases/download/v3.12.1/DB.Browser.for.SQLite-3.12.1-win64-v2.msi):

![sqlitebrowser](sqlitebrowser.gif)

Note que o ORMLite criou duas tabelas, uma tabela *student* que possui os dados de cada objeto *student* e uma tabela *sqlite_sequence* que controla o *id* auto incrementado, no nosso exemplo, o último *id* da tabela *student* tem valor 2, que identifica a estudante "Maria Souza".

Com estas 3 classes, concluímos a camada de persistência e nossos componentes *Model*. 

### 6.2. View: interface gráfica do Estudante

Para se criar uma interface gráfica visualmente, podemos utilizar a [IDE](https://pt.wikipedia.org/wiki/Ambiente_de_desenvolvimento_integrado#:~:text=IDE%2C%20do%20ingl%C3%AAs%20Integrated%20Development,objetivo%20de%20agilizar%20este%20processo.) [Netbeans](https://netbeans.apache.org/). Para montar a interface de cadastro de estudante, será construído dois painéis:
* **Formulário de cadastro de estudante**: este formulário contém basicamente rótulos (*labels*) e campos de texto (*textfield*).
* **Botões de formulário:** um painel composto de botões que permitem operar um repositório de objetos. Além das operaões básicas CRUD (Create, Retrieve, Update e Delete), também serão utilizados botões de navegação de registros, com os botões: primeiro (*first*), anterior (*previous*), próximo (*next*) e último (*last*) registros.

Estes dois componentes gráficos podem ser então incluídos em um painel, que será o componente de visão (*StudentRepositoryView*) do repositório de estudantes (*StudentRepository*).

#### 6.2.1. Painel: formulário de cadastro de estudante (*StudentFormPanel*)

O formulário de estudantes pode ser criado no Netbeans, criando-se um projeto antes, que arbitrariamente chamaremos de *crud_mvc_ap2* (opção de menu File > New Project e opção "*Java Application*"). O nome do pacote é também arbitrário, constuma-se atribuir o nome inverso do domínio da empresa do autor do pacote seguido do nome do projeto, por exemplo "br.inf.ufg.crud_mvc_ap2". Uma vez criado o projeto/pacote, pode-se criar uma nova classe (opção Source Packages > br.ufg.inf.crud_mvc_ap2 > New JPanel Form), o nome da classe arbitrariamente será "StudentFormPanel". O Netbeans gera uma subclasse de [javax.swing.JPanel](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/swing/JPanel.html) com layout [GroupLayout](https://docs.oracle.com/javase/tutorial/uiswing/layout/group.html), que é o padrão do Netbeans e permite organizar os componentes internos do painel (rótulos e campos de texto) de forma poderosa e simples:

![netbeans projects](netbeans_project.gif)

O passo seguinte é criar o formulário de cadastro de estudante:

![](studentform.gif)

Como os campos de textos serão acessados futuramente pelo controlador, é conveniente atribuir nomes para cada um, por exemplo: *tfId*, *tfFullname*, *tfRegistration* e *tfBirthday*. O prefixo arbitrário 'tf' é relacionado ao nome do componente (*[JTextField](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/swing/JTextField.html)*).

Com a tela gerada, pode-se copiar e colar o código-fonte gerado no Netbeans para testar no BlueJ:

![](studentform_bluej.gif)

Note nos testes do BlueJ, que a compilação inicial funciona bem, mas depois que se geram os *getters* e *setters*, a compilação falha e é necessário importar as classes *JLabel* e *JTextField*. Outro ponto importante é que o *JPanel* não é visualizável diretamente quando é instanciado, é necessário criar uma instância de [*JFrame*](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/swing/JFrame.html), e então adicioná-lo através do método *add*:

```java
javax.swing.JFrame janela = new javax.swing.JFrame();
janela.setSize(300,300); // tamanho da janela
janela.setVisible(true); // torna a janela visível
janela.setLocation(500,500); // move a janela para coordenada (500,500)
janela.add(studentF1);
janela.pack(); // redimensiona os elementos internos da janela e renderiza-os novamente
```

#### 6.2.2. Painel: botões de formulário (*ButtonFormPanel*)

Um componente bastante recorrente e útil nas aplicações CRUD é um painel de botões que controlam as operações de criar, recuperar, atualizar e deletar objetos. No sentido de facilitar o reuso deste componente gráfico, é necessário utilizar um contêiner do tipo [JPanel](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/swing/JPanel.html) (painel) em vez de uma [JFrame](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/swing/JFrame.html) (janela), o primeiro foi criado para ser reusado em outros contêineres e o segundo como um componente independente que não pode ser embutido em outro contêiner. Siga os seguintes passos para criar a classe *ButtonFormPanel*:

![](button-form-panel.gif)

Note que ícones podem ser encontrados no próprio sistema (/usr/share/icons/gnome/32x32/ no Ubuntu) ou na [Web](https://fontawesome.com/icons?d=gallery&p=2). O próximo passo é copiar as imagens para o BlueJ, compilar o código-fonte e adaptá-lo conforme feito na classe anterior e testar:

![](button-form-panel-bluej.gif)

#### 6.2.3. Painel: cadastro de estudante (*StudentRepositoryView*)

Nas seções anteriores, criamos dois painéis: *StudentFormPanel* e *ButtonFormPanel*, agora podemos utilizá-los para criar um outro painel que cumprirá com o papel de visão do repositório de estudante. O processo é o mesmo das seções anteriores, no entanto, se desejarmos visualizar os painéis anteriores neste painel, precisamos de importá-los para a paleta de componentes. No Netbeans, para que painéis possam ser arrastáveis a partir da paleta, é necessário construir a aplicação, gerando-se um arquivo .jar e que depois possa ser incluído na paleta por meio de um  gerenciador. Siga o processo:

![](student-repository-view.gif)

Para importar o painel no BlueJ, siga o mesmo processo anterior executado nos painéis anteriores:

![](student-repository-view-bluej.gif)

Note no exemplo acima, que para facilitar os testes, criamos uma função *main* que instancia um painel *StudentRepositoryView* e o inclui em uma janela JFrame. Com isso, já temos a visão e o modelo, falta apenas criar o controlador.

### 6.3. Controller: controle do repositório do estudante (*StudentRepositoryController*)

Como foi dito, o controlador contém toda lógica de um MVC. É o controlador que decide o que deve ser apresentado na visão e controla todos os dados de e para o modelo. O controlador controla o estado do modelo, recebendo ações da interface gráfica por meio do usuário. Uma forma visual de modelar o estado de um modelo e suas transições é o [Diagrama de Estados](https://pt.wikipedia.org/wiki/Diagrama_de_transi%C3%A7%C3%A3o_de_estados):  

```plantuml
title <b>Diagrama de Estado de um Formulário que interage com Repositório</b> 
[*] --> INITIAL
INITIAL --> SEARCHING : bSearch
INITIAL --> CREATING : bCreate
INITIAL: Enabled: bSearch, bCreate \nEmpty: tfAll*
SEARCHING --> INITIAL : bCancel
SEARCHING: Enabled: bOK, bCancel \nEmpty: tfAll*
CREATING --> INITIAL : bCancel
CREATING: Enabled: bOK, bCancel, tfAllExcId*\nEmpty:tfAll*
SEARCHING --> SEARCHING: bOK && not found \n (0 registers)
SEARCHING --> LOADED : bOK && found >= 1 register(s)
CREATING --> CREATING : bOK && input error
CREATING --> LOADED : bOK && 1 register created
state LOADED {
  [*] --> LOADED_FIRST: >1 registers
  [*] --> LOADED_ONE: 1 register
  LOADED_ONE: Enabled: bUpDelCancel*, lRegisters
  LOADED_FIRST: Enabled: bUpDelCancel*, lRegisters, \nbNext, bLast
  LOADED_FIRST --> LOADED_LAST : bLast
  LOADED_FIRST --> LOADED_MIDDLE : bNext \n&& isLast==false
  LOADED_MIDDLE: Enabled: bUpDelCancel*, lRegisters, \nbNavigation*
  LOADED_MIDDLE --> LOADED_FIRST: bFirst
  LOADED_MIDDLE --> LOADED_MIDDLE: bNext \n&& isLast==false
  LOADED_MIDDLE --> LOADED_LAST: bNext \n&& isLast==true 
  LOADED_MIDDLE --> LOADED_MIDDLE: bPrevious \n&& isFirst==false
  LOADED_MIDDLE --> LOADED_FIRST: bPrevious \n&& isFirst==true  
  LOADED_MIDDLE --> LOADED_LAST: bLast 
  LOADED_LAST: Enabled: bUpDelCancel*, lRegisters, \nbFirst, bPrevious
  LOADED_LAST --> LOADED_FIRST: bFirst
  LOADED_LAST --> LOADED_MIDDLE: bPrevious \n&& isFirst==false
  
}
LOADED --> UPDATING : bUpdate
LOADED --> DELETING : bDelete
LOADED --> INITIAL : bCancel
UPDATING --> LOADED : bCancel
UPDATING --> INITIAL : bOK
UPDATING --> UPDATING : bOK && input error
UPDATING: Enabled: bOK, bCancel, tfAllExcId*
DELETING --> LOADED : bCancel
DELETING --> INITIAL : bOK
DELETING: Enabled: bOK, bCancel 
```

O diagrama define 5 estados, sendo 4 sub-estados de *LOADED*,  que um modelo de repositório pode assumir:
* **INITIAL:** estado inicial do repositório, em que somente os botões *bSearch* e *bCreate* estão habilitados (Enabled) e todos os campos de textos - *textfields* - (*tfAll**) estão vazios (*Empty*). As setas que saem deste estado, indicam a transição quando algum ação é realizada:
  * *bSearch*: muda para o estado *SEARCHING*
  * *bCreate*: muda para o estado *CREATING*
* **SEARCHING:** estado em que uma busca está sendo realizada. Neste estado, somente os botões *bOK* e *bCancel* vão estar habilitados e quando são acionados, há transição ou mudança de estado. Neste estado, todos os campos de texto (*tfAll**) inicialmente vão estar vazios. As transições de estado ocorrem pelas seguintes condições:
  * *bCancel*: retorna para o estado *INITIAL*.
  * *bOK && not found*: nenhum registro foi encontrado (0 registers found), continua no estado *SEARCHING*
  * *bOK && >= 1 register(s)*: foi(ram) encontrado(s) 1 ou mais registros, muda para o estado *LOADED*
* **CREATING:** estado em que um novo cadastro está sendo realizado. Neste estado, somente os botões *bOK* e *bCancel* vão estar habilitados e quando são acionados, há transição ou manutenção de estado. Todos os campos de texto vão estar habilitados, exceto o *tfId* (*tfAllExcId**), que corresponde ao identificador (*id*) auto-gerado e o seu controle deve ser exclusivamente realizado pelo banco de dados. Todos os campos de texto (*tfAll**) vão estar vazios para poder receber a entrada de dados do usuário. Se o usuário acionar o botão bCancel, o repositório retornará ao estado *INITIAL*. Para o botão *bOK*, a transição ocorrerá caso se confirme ou não uma entrada válida:
  * *bOK && input error*: o usuário forneceu uma entrada inválida e continua no estado *CREATING*.
  * *bOK && register created*: o usuário criou um registro bem sucedido e transita para o estado LOADED, mais especificamente no estado *LOADED_ONE*.
* **LOADED:** estado em que o repositório tem um ou mais registros carregados. Este é o estado mais complexo, pois contém 4 sub-estados, que apresenta o registro atual e a quantidade de registros carregados no objeto *lRegisters* (*row X of Y rows*). Note neste caso, temos um estado geral (LOADED) que transitará para os estados *INITIAL*, *UPDATING* ou *DELETING*, quando for acionado um dos botões *bCancel*, *bUpdate* ou *bDelete*, respectivamente. Quando nenhum destes botões forem acionados, o repositório transitará entre os sub-estados, seguem a descrição de cada um:
  * **LOADED_ONE:** estado em que o repositório contém somente um registro carregado. Somente os botões *bUpDelCancel** (bUpdate, bDelete e bCancel) e o rótulo de registros (*lRegisters*) vão estar habilitados.
  * **LOADED_FIRST:** estado em que mais de um registro foi carregado, sendo que os dados do primeiro registro vão ser mostrados no formulário. Somente os botões *bUpDelCancel**, *bNext*, *bLast* e o rótulo *lRegisters* vão estar habilitados. O acionamento do botão *bLast* transita para o estado *LOADED_LAST*, enquanto que se o acionamento do botão *bNext* e se o registro atual não for o último (*isLast == false*) a transição será para *LOADED_MIDDLE*.
  * **LOADED_MIDDLE:** estado em que o registro atual não é nem o primeiro, nem o último.  Somente os botões *bUpDelCancel**,  os botões de navegação (*bNavigation**) e o rótulo *lRegisters* vão estar habilitados. O acionamento de um dos botões de navegação e as condições de último registro (*isLast==true*) ou primeiro registro (*isFirst==true*) vão determinar as transições de estado conforme o diagrama.
  * **LOADED_LAST:** estado em que mais de um registro foi carregado, sendo que os dados do último registro vão ser apresentados no formulário. Os detalhes de transições de estados estão representados no diagrama.

**UPDATING** e **DELETING** são estados originados de um estado **LOADED**, pois somente pode-se atualizar ou deletar um registro se ele estiver carregado previamente no repositório. Os detalhes destes dois estados estão representados no diagrama.

Este diagrama de estados foi editado na ferramenta [PlantUML](https://plantuml.com/) e o [código-fonte está disponível embutido neste documento](https://gitlab.com/-/ide/project/ap2-ec-2020-2/tutorials/tree/master/-/crud_mvc/README.md/#L411) ou [online](https://www.planttext.com/api/plantuml/img/ZLJBZfim5Dtp5NSrcY96RRTe4Wa5fqL3an59LcrL6N1IIxngp6YggX_JR-Wdp8_LF0msa0uhmETnZw_nvPGd1Ex3jKlnXU4KGqm0bHp7UVrMfR3DMLebBtyOpU57HO1cd31y8H3bAGIao4lAN_uAzFvjk0RZi_aLxkxMuEsye-VuHlTirWx82JOVlTq7i24y4CoYxng-2P1pxE0D8vWJ2Tk0CXmc9Ew5bgJ0bmob1Vzf0pyxIM8QGnKbHsE9iuWa0wwQxXyiIPWOzh7c_IHynSvgRT1pvCMcj9wDgWGJ1d1p0rdEuPnNMImomUqxOEH2Iz7-yir8wkyT5xdG2rlHUWNlUysj46axKRlTgcXML1m8OpdJgMFxmHMYvX1YG_mouYmwuYy3GFu8xTAthHSSZZQi1sav9Urto5RC1K45XlQU2fSaRNjD2v90STJBlIwnH6l37Ndcui1yNFAHXPhETyIt_Ihja-UwFgg9jQsmhpjRrgBLweoJaWpQbhey9dwY5ynfdfbZZvbMXrlArA1JeYo_DEYu6RDApiIGM5JraP4dcbUbr3Tv5vIM-xkYRmByDq6NlSwhyDhLXKVHrBA63BhDWjCOqvPsnVXjT9Ui5fyUtVuEdugudfWAwY8VILJiXUZeT9BrRlgTdqCLyVv1XzHCqxaYqINJqkWtC1j8G_L04hesuly1).

#### 6.3.1. Inclusão da propriedate state no modelo

A implementação dos estados no modelo pode ser feita por [Java Enums](http://tutorials.jenkov.com/java/enums.html), que consistem em coleções de constantes. No trecho de código-fonte abaixo, foi criado todas os estados na enum *State*, e o conjunto ([java.util.Set](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Set.html)) *LOADED* foi definido como sendo um conjunto de Enums ([java.util.EnumSet](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/EnumSet.html)) do tipo *State*. A propriedade *state* foi inicializada com o estado *INITIAL*.

```java
// outros imports
import java.util.Set;
import java.util.EnumSet;

public class StudentRepository {
  // ...outros atributos
  public enum State { INITIAL, SEARCHING, CREATING, DELETING, 
    UPDATING, LOADED_ONE, LOADED_FIRST, LOADED_MIDDLE, LOADED_LAST };
  public static Set<State> LOADED = EnumSet.of(State.LOADED_ONE, State.LOADED_FIRST,
                            State.LOADED_LAST, State.LOADED_MIDDLE);      
  private State state = State.INITIAL;

  // outros métodos
}
```

#### 6.3.2. Implementação do estado *INITIAL*

Como pode ser notado no diagrama de estados, na transição de estados é necessário habilitar ou desabilitar grupos de componentes de botões ou campos de texto. Para facilitar esse processo, podemos definir listas de componentes no painel de botões (*ButtonFormPanel*) e no formulário de estudante (*StudentFormPanel*):

```java
public class ButtonFormPanel extends javax.swing.JPanel {
  public ButtonFormPanel() {
      initComponents();
      navigationButtons = java.util.Arrays.asList(bFirst, bNext, bPrevious, bLast);
      okCancelButtons = java.util.Arrays.asList(bOK, bCancel);
      controlButtons = java.util.Arrays.asList( bSearch, bCreate, bDelete, bUpdate, bCancel, bOK );
      searchCreateButttons = java.util.Arrays.asList(bSearch, bCreate );
      deleteUpdateButttons = java.util.Arrays.asList(bDelete, bUpdate );    
  }
// ...
  private java.util.List<javax.swing.JComponent> navigationButtons;
  private java.util.List<javax.swing.JComponent> okCancelButtons; 
  private java.util.List<javax.swing.JComponent> controlButtons; 
  private java.util.List<javax.swing.JComponent> searchCreateButttons; 
  private java.util.List<javax.swing.JComponent> deleteUpdateButttons;
// ...
  public java.util.List<javax.swing.JComponent> getNavigationButtons(){
        return this.navigationButtons;
  }//end method getNavigationButtons
  // outros métodos getters
}
// ...
public class StudentFormPanel extends javax.swing.JPanel {
  public StudentFormPanel() {
      initComponents();
      allFields = java.util.Arrays.asList(tfId, tfBirthday, tfFullname, tfRegistration);
  }
  //...
  private java.util.List<javax.swing.JComponent> allFields;
  //...
  public java.util.List<javax.swing.JComponent> getAllFields(){
      return this.allFields;
  }//end method getAllFields
  // outros métodos getters
}
public class StudentRepositoryController {
  public StudentRepositoryController(StudentRepository model, StudentRepositoryView view)
  {
      this.model = model;
      this.view = view;
      init();
  }
  public void init() {
      model.setStatus(StudentRepository.Status.INITIAL);
      updateView();
  }
  private void enableComponents(List<JComponent> components) {
      for (JComponent component : components)
          component.setEnabled(true);
  }
  private void disableComponents(List<JComponent> components) {
      for (JComponent component : components)
          component.setEnabled(false);
  } 
  private void clearFields(List<JComponent> components) {
      for (JComponent component: components)
          ((javax.swing.JTextField) component).setText("");
  }
  public void updateView() {
    if (model.getStatus() == StudentRepository.Status.INITIAL ) {
        disableComponents(view.getButtonFormPanel().getNavigationButtons());
        disableComponents(view.getButtonFormPanel().getOkCancelButtons());
        disableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
        enableComponents(view.getButtonFormPanel().getSearchCreateButttons());
        clearFields(view.getStudentFormPanel().getAllFields());
        disableComponents(view.getStudentFormPanel().getAllFields());            
    }
  } 
}
```

Note no código acima, que as listas de componentes são declaradas como *java.util.List<javax.swing.JComponent>*. O tipo de lista declarado é genérico, pois a classe *javax.swing.JComponent* é uma superclasse comum às duas subclasses concretas que serão incluídas nessas listas: *javax.swing.JButton* (botões) e *javax.swing.JTextField* (campos de texto), isso é um exemplo de aplicação do [polimorfismo](https://pt.wikipedia.org/wiki/Polimorfismo_(ci%C3%AAncia_da_computa%C3%A7%C3%A3o)).

Utilizando polimorfismo, listas genéricas de componentes podem receber tipos variados, como botões ou campos de texto, com isso podemos inicializar as listas genéricas com tipos específicos:

```java
...
navigationButtons = java.util.Arrays.asList(bFirst, bNext, bPrevious, bLast);
...
allFields = java.util.Arrays.asList(tfId, tfBirthday, tfFullname, tfRegistration);
...
```

Também podemos aplicar o método *setEnabled*, comum a todas subclasses de componentes: 

```java
...
  private void enableComponents(List<JComponent> components) {
      for (JComponent component : components)
          component.setEnabled(true);
  }
...
```

Mas no caso de campos de texto, é necessário realizar um [downcast](https://www.baeldung.com/java-type-casting), conversão de superclasse (JComponent) para subclasse (JTextField):

```java
  private void clearFields(List<JComponent> components) {
      for (JComponent component: components)
          ((javax.swing.JTextField) component).setText("");
  }
```

Para realizar a execução do controlador, é conveniente criar um método *main*:

```java
  public static void main(String[] args) {
      StudentRepository model = new StudentRepository(new Database(args[0]));
      StudentRepositoryView view = new StudentRepositoryView();
      StudentRepositoryController studentRepositoryController = new StudentRepositoryController(model, view);
      javax.swing.JFrame jFrame = new javax.swing.JFrame();
      jFrame.setSize(900,600);
      jFrame.setLayout(new java.awt.BorderLayout());
      jFrame.add(view, java.awt.BorderLayout.CENTER);
      jFrame.setVisible(true);
    }
```

No método *main* acima, o nome da base de dados é informada na *array* args e a *view* é incluída na janela (*jFrame*). Segue uma demonstração da implementação do estado *INITIAL* no controlador:

![](initial.gif)

#### 6.3.3. Implementação do estado *CREATING*

A implementação do estado *CREATING* envolve a modificação dos métodos *init* e *updateView*, bem como a criação dos métodos *creating*, *commit* e *cancel*:
* *init:* a modificação envolve a ligação de um evento que ocorre no componente visual para um método do controlador, por exemplo:
```java
public void init() {
  //...    
  view.getButtonFormPanel().getBCreate().addActionListener(e -> creating());
  /...
}
```
  * A linha de código acima, liga o evento de acionamento do botão *bCreate* ao método *creating*. O método [*actionListener*](https://docs.oracle.com/javase/tutorial/uiswing/events/actionlistener.html) é um método suportado a quase todos componentes gráficos e exige que seja informado um método que trate o evento. A forma mais simples e direta é usar [expressões lambda](https://www.oreilly.com/content/lambda-expressions/), no qual se informa uma variável (arbitrariamente nomeada como 'e'  no exemplo, que será do tipo [java.awt.AWTEvent](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/java/awt/AWTEvent.html)) seguida de uma seta (->) e o nome do método local. No exemplo acima, o evento de acionamento do botão é tratado pelo método *creating* local.

As modificações e os métodos a serem criados estão aqui:

```java
// outros imports...
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

public class StudentRepositoryController {
  public void init() {
      model.setStatus(StudentRepository.Status.INITIAL);
      view.getButtonFormPanel().getBCreate().addActionListener(e -> creating());
      view.getButtonFormPanel().getBOK().addActionListener(e -> commit());
      view.getButtonFormPanel().getBCancel().addActionListener(e -> cancel());
      updateView();
  }
  //...
  public void updateView() {
    if (model.getStatus() == StudentRepository.Status.INITIAL ) {
      //...
    } else if (model.getStatus() == StudentRepository.Status.CREATING ) {
      disableComponents(view.getButtonFormPanel().getNavigationButtons());
      enableComponents(view.getButtonFormPanel().getOkCancelButtons());
      disableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
      disableComponents(view.getButtonFormPanel().getSearchCreateButttons());
      enableComponents(view.getStudentFormPanel().getAllFields());
      view.getStudentFormPanel().getTfId().setEnabled(false);
    } 
  }   
  //...
  public void creating() {
      model.setStatus(StudentRepository.Status.CREATING);       
      updateView();
  }
  //...
  public void commit() {
    if ( model.getStatus() == StudentRepository.Status.CREATING ) {
        try {
            Student studentLoaded = loadFromView();
            Student newStudent = model.create(studentLoaded);
            JOptionPane.showMessageDialog(view, "Student saved, with id: "+newStudent.getId());
            model.setStatus(StudentRepository.Status.LOADED_ONE);
            updateView();
        } catch(Exception e) {
            JOptionPane.showMessageDialog(view,"Error: Student not saved. Details: "+e);
        } 
    }    
  }
  public void cancel() {
    if ( model.getStatus() == StudentRepository.Status.CREATING ) {
        try {
            int response = JOptionPane.showConfirmDialog(view, "Do you want to cancel and clear all fields?");
            if (response == JOptionPane.YES_OPTION ) {
                model.setStatus(StudentRepository.Status.INITIAL);
                updateView();
            }
        } catch(Exception e) {
            JOptionPane.showMessageDialog(view,"Error: Student not saved. Detais: "+e);
        }
    } else if ( model.getStatus() == StudentRepository.Status.CREATING ) {
      model.setStatus(StudentRepository.Status.INITIAL);
      updateView();
    }
  }    
     
  private java.util.Date stringToDate( String string ) throws Exception {
      if ( string.equals("") )
          return null;
      Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
      Matcher matcher = dataPadrao.matcher(string);
      if (matcher.matches()) {
          int dia = Integer.parseInt(matcher.group(1));
          int mes = Integer.parseInt(matcher.group(2));
          int ano = Integer.parseInt(matcher.group(3));
          return (new GregorianCalendar(ano,mes,dia)).getTime();
      }
      else throw new Exception("Invalid data format");        
  }    

  private int stringToInt( String string ) {
      return string.equals("") ? 0 : Integer.parseInt(string);
  }

  private Student loadFromView() throws Exception {
      Student studentLoaded = new Student();
      String strId = view.getStudentFormPanel().getTfId().getText();
      int id = stringToInt(strId);
      studentLoaded.setId(id);
      studentLoaded.setFullName(view.getStudentFormPanel().getTfFullname().getText());
      String strRegistration = view.getStudentFormPanel().getTfRegistration().getText();
      int registration = stringToInt(strRegistration);
      studentLoaded.setRegistration(registration);
      studentLoaded.setBirthday(stringToDate(view.getStudentFormPanel().getTfBirthday().getText()));
      return studentLoaded;
  }    
```

O método *creating* é acionado pelo botão *bCreate*, que altera o estado do modelo para *CREATING* e atualiza a visão, conforme definido no diagrama de estados. Os métodos *cancel* e *commit* são acionados pelos botões *bCancel* e *bOK* respectivamente e caso o estado atual seja *CREATING*, realizam as ações de transições de estado:
* *cancel*: confirma se o usuário realmente quer cancelar a criação de registro, caso sim, retorna para o estado INITIAL, limpando todos os campos de texto.
* *commit*: carrega os dados da visão através do método *loadFromView* para um objeto *studentLoaded* e o salva através do método *create* do *model*, salvando um novo objeto estudante *newStudent*. A diferença entre os dois objetos é que o último possui um *id* atribuído pelo banco de dados.

Segue a demonstração da implementação do estado *CREATING:*

![](creating.gif)

#### 6.3.4. Implementação do estado *SEARCHING*

A implementação do estado *SEARCHING*, começa na ligação do via método *addActionListener* do evento do botão *bSearch* para o método *searching*. Assim, caso o usuário acione o botão *bSearch*, o método *searching* é executado, que simplesmente muda o estado do modelo para *SEARCHING* e depois invoca o método *updateView*, que atualiza a visão conforme definido no diagrama de estados.

```java
// ...
  public void init() {
    // outras inicializações de objeto
    view.getButtonFormPanel().getBSearch().addActionListener(e -> searching());
    // outras inicializações de objeto
  }
  public void searching() {
      model.setStatus(StudentRepository.Status.SEARCHING);
      updateView();
  }
// ...
```

No estado *SEARCHING*, os botões *bOK* e *bCancel* estão habilitados e o tratamento de evento destes botões estão associados os métodos *commit* e *cancel*, respectivamente. A busca será por *id* específico ou não; caso nenhum *id* seja informado, a busca será por todos os registros do repositório. 

No método *commit*, em conformidade com o diagrama de estados, o tratamento da transição de *SEARCHING* para *LOADED_ONE* ou *LOADED_FIRST* vai depender da quantidade de elementos retornados da busca. Segue o código-fonte completo das modificações necessárias para implementar o estado *SEARCHING*:

```java
public class StudentRepositoryController
{
  public void init() {
    // outras inicializações de objeto
    view.getButtonFormPanel().getBSearch().addActionListener(e -> searching());
    // outras inicializações de objeto
  }
  public void searching() {
      model.setStatus(StudentRepository.Status.SEARCHING);
      updateView();
  }
  public void updateView() {
    if (model.getStatus() == StudentRepository.Status.INITIAL ) {
      // implementação da visão no estado INITIAL
    } else if (model.getStatus() == StudentRepository.Status.CREATING ) {
      //  implementação da visão no estado CREATING
    } else if (model.getStatus() == StudentRepository.Status.SEARCHING ) {
      disableComponents(view.getButtonFormPanel().getNavigationButtons());
      enableComponents(view.getButtonFormPanel().getOkCancelButtons());
      disableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
      disableComponents(view.getButtonFormPanel().getSearchCreateButttons());
      disableComponents(view.getStudentFormPanel().getAllFields());
      view.getStudentFormPanel().getTfId().setEnabled(true);
      clearFields(view.getStudentFormPanel().getAllFields());            
    } else if (model.getStatus() == StudentRepository.Status.DELETING ) {
      // implementação da visão no estado DELETING
    }
    else if (model.getStatus() == StudentRepository.Status.UPDATING ) {
      // implementação da visão no estado UPDATING
    }        
  }
  private String dateToString( java.util.Date date ) {
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
  }    
  public void loadFromModel() throws Exception {
    Student studentLoaded = model.getLoadedStudent();
    if (studentLoaded == null)
        throw new Exception("Student not loaded");
    view.getStudentFormPanel().getTfId().setText(String.valueOf(studentLoaded.getId()));
    view.getStudentFormPanel().getTfFullname().setText(studentLoaded.getFullName());
    view.getStudentFormPanel().getTfRegistration().setText(String.valueOf(studentLoaded.getRegistration()));
    view.getStudentFormPanel().getTfBirthday().setText(dateToString(studentLoaded.getBirthday()));
    List<Student> studentsLoaded = model.getLoadedStudents();
    if (studentsLoaded == null)
        throw new Exception("Repository is not loaded");
    int size = studentsLoaded.size();
    int id = studentsLoaded.indexOf(studentLoaded);
    view.getButtonFormPanel().getLRegisters().setText("row "+(id+1)+" of "+size+" rows");
  }
        
  public void commit() {
    if ( model.getStatus() == StudentRepository.Status.CREATING ) {
      // ...
    } else if ( model.getStatus() == StudentRepository.Status.SEARCHING ) {
      try {
        Student studentLoaded = loadFromView();
        if ( studentLoaded.getId() == 0 ) {
            List<Student> studentsFound = model.loadAll();
            if ( studentsFound.size() == 1 ) {
                JOptionPane.showMessageDialog(view, "Student found: "+studentsFound.size()+" row");
                model.setStatus(StudentRepository.Status.LOADED_ONE );
            } else if( studentsFound.size() > 1) { 
                JOptionPane.showMessageDialog(view, "Students found: "+studentsFound.size()+" rows");
                model.setStatus(StudentRepository.Status.LOADED_FIRST );
            } else
                JOptionPane.showMessageDialog(view, "Students not found, repository is empty");
        } else {
            Student studentFound = model.loadFromId( studentLoaded.getId() );
            if ( studentFound != null ) {
                JOptionPane.showMessageDialog(view, "Student found with id: "+studentFound.getId());
                model.setStatus(StudentRepository.Status.LOADED_ONE);
            }
            else
                JOptionPane.showMessageDialog(view, "Student not found");
        }
        updateView();
      } catch (Exception e) {
          JOptionPane.showMessageDialog(view, "Error: Searching not performed. Details: "+e);
      }    
    }
  }
} // fim da classe
```

Segue a demonstração da implementação do estado *SEARCHING*:

![](searching.gif)

#### 6.3.5. Implementação do estado *LOADED*

Para implementar o estado *LOADED*, é necessário implementar alguns métodos que permitem navegar pelos *caches* *loadedStudent* e *loadedStudents*, que são objetos que representam um estudante (*Student*) e uma lista de estudantes (*List&lt;Student&gt;*), respectivamente. Estes métodos serão úteis para realizar as operações disparadas pelos botões de navegação (*bFirst*, *bPrevious*, *bNext* e *bLast*), por isso, no modelo serão implementados os métodos *loadFirst*, *loadPrevious*, *loadNext* e *loadLast*, respectivamente. Também serão implementados os métodos *loadedFirst* e *loadedLast*, que verificam se o estudante atual é o primeiro ou último da lista, respectivamente. Seguem os métodos a serem incluídos na classe *StudentRepository*:

```java
public class StudentRepository {
// ...
    public void loadFirst() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        setLoadedStudent(loadedStudents.get(0));
    }    
    
    public void loadPrevious() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        int index = loadedStudents.indexOf(loadedStudent);
        if ( index == 0 )
            throw new Exception("Could not loaded previous student, current index is 0");
        setLoadedStudent(loadedStudents.get(index-1));
    }

    public void loadNext() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        int index = loadedStudents.indexOf(loadedStudent);
        if ( index == loadedStudents.size()-1 )
            throw new Exception("Could not loaded next student, current index is the last");
        setLoadedStudent(loadedStudents.get(index+1));
    }
    
    public void loadLast() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        int last = loadedStudents.size()-1;
        setLoadedStudent(loadedStudents.get(last));
    }        
    
    public boolean loadedLast() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        if ( loadedStudents.indexOf(loadedStudent)+1 == loadedStudents.size() )
            return true;
        else
            return false;
    }
    
    public boolean loadedFirst() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        if ( loadedStudents.indexOf(loadedStudent) == 0 )
            return true;
        else
            return false;
    }
// ...
}
```

Estes métodos podem ser testados no BlueJ, segue uma demonstração:

![](loading.gif)

O próximo passo é implementar o conjunto de estados *LOADED** no controlador. No método *init*, podemos associar todos eventos de botões de navegação para um método geral *loading*, informando a ação do botão:

```java
public class StudentRepositoryController {
  // ...
  public void init() {
    //...
        view.getButtonFormPanel().getBFirst().addActionListener(e -> loading("FIRST"));
        view.getButtonFormPanel().getBPrevious().addActionListener(e -> loading("PREVIOUS"));          
        view.getButtonFormPanel().getBNext().addActionListener(e -> loading("NEXT"));
        view.getButtonFormPanel().getBLast().addActionListener(e -> loading("LAST"));
    // ...
  }
  //...
  public void loading(String action) {
    try {
        switch(action) {
            case "PREVIOUS":
              model.loadPrevious();
              if ( model.loadedFirst() )
                  model.setState(StudentRepository.State.LOADED_FIRST);
              else
                  model.setState(StudentRepository.State.LOADED_MIDDLE);
            break;
            case "NEXT":
                model.loadNext();
                if ( model.loadedLast() )
                  model.setState(StudentRepository.State.LOADED_LAST);
                else
                  model.setState(StudentRepository.State.LOADED_MIDDLE);
            break;
            case "LAST":
                model.loadLast();
                model.setState(StudentRepository.State.LOADED_LAST);
            break;
            case "FIRST":
              model.loadFirst();
              model.setState(StudentRepository.State.LOADED_FIRST);
            break;
        }
    }
    catch (Exception e) {
        System.out.println("Error on loading. Details: "+e);
    }
    updateView();
  }
}
```

Para atualizar a visão, temos que tratar quando é um dos estados do conjunto *LOADED**. Para fazer isso, podemos utilizar o método [*contains*](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Set.html#contains(java.lang.Object)) que verifica se o estado pertence ao conjunto *LOADED**. Caso sim, podemos aplicar todas atualizações de estado comuns a todos estados do conjunto, conforme o diagrama e depois aplicar atualizações específicas para os estados *LOADED_FIRST*, *LOADED_LAST* e *LOADED_ONE*:

```java
    public void updateView() {
        if (model.getState() == StudentRepository.State.INITIAL ) {
          // ...
        } else if (model.getState() == StudentRepository.State.CREATING ) {
          // ...
        } else if (model.getState() == StudentRepository.State.SEARCHING ) {
          // ...           
        } else if (model.getState() == StudentRepository.State.DELETING ) {
            // implementação da visão no estado DELETING
        } else if (model.getState() == StudentRepository.State.UPDATING ) {
            // implementação da visão no estado UPDATING
        }
        else if ( StudentRepository.LOADED.contains(model.getState()) ) {
            try {
                loadFromModel();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(view, "Error: Repository not loaded. Details: "+e);
            }
            enableComponents(view.getButtonFormPanel().getNavigationButtons());
            disableComponents(view.getButtonFormPanel().getOkCancelButtons());
            view.getButtonFormPanel().getBCancel().setEnabled(true);
            enableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
            disableComponents(view.getButtonFormPanel().getSearchCreateButttons());
            disableComponents(view.getStudentFormPanel().getAllFields());
            if ( model.getState() == StudentRepository.State.LOADED_ONE ) {
                disableComponents(view.getButtonFormPanel().getNavigationButtons());
            } else if ( model.getState() == StudentRepository.State.LOADED_FIRST ) {
                view.getButtonFormPanel().getBFirst().setEnabled(false);
                view.getButtonFormPanel().getBPrevious().setEnabled(false);
            } else if ( model.getState() == StudentRepository.State.LOADED_LAST ) {
                view.getButtonFormPanel().getBLast().setEnabled(false);
                view.getButtonFormPanel().getBNext().setEnabled(false);
            }
        }   
    }

```

Segue a demonstração da implementação deste estado:

![](navigating.gif)

### 6.4. Aplicação: controle dos controladores

Conforme visto na seção 4, um dos desafios da programação de uma aplicação que segue o padrão MVC é evitar que o sistema fique demasiadamente complexo e prejudique a sua manutenção. Com a evolução da aplicação, vão se criando mais e mais "MVCs", proporcionalmente a quantidade de entidades (Estudante, Professor, Mensagem, etc...) e para não ter problemas futuros, deve-se evitar que um MVC comunique com outro MVC diretamente, trabalhando-se com uma aplicação geral (*App*) que controla todos os outros controladores.

Para se fazer isso, o primeiro passo é criar o modelo:

```java
import java.io.File;
/**
 * Escreva a descrição da classe AppModel aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class AppModel
{
    private File file;
    private Database database;
    public enum Status { DB_CLOSED, DB_OPEN }
    private Status status;

    // getters e setters ...
}
```

Nosso modelo basicamente armazenará as seguintes informações: o objeto de arquivo(*file*), a base de dados (*database*) e o status da aplicação (*DB_CLOSED* ou *DB_OPEN*).

O segundo passo é desenhar a interface, criando-se uma visão da aplicação (*AppView*). No desenho da interface, é importante em escolher como o arquivo de base de dados será aberto. Arbitrariamente, foi escolhido um [menu](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/swing/JMenu.html), com duas opções: *File > New* ou *File > Open*, considerando-se que teríamos abrir um novo arquivo ou escolher um arquivo previamento criado, através de um diálogo de escolha de arquivo, chamado [JFileChooser](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/swing/JFileChooser.html). Também foi pensado em uma outra oção de menu *Repository > Student*, no sentido de abrir o formulário de cadastro de estudante em um [painel de abas](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/swing/JTabbedPane.html). Podemos também utilizar um rótulo ([JLabel](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/swing/JLabel.html)) ocupando a borda inferior da janela ([JFrame](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/swing/JFrame.html)), com o painel de abas ocupando todo o centro em um layout de bordas ([BorderLayout](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/java/awt/BorderLayout.html)).

Segue uma animação de como construir o *AppView*:

![](appview.gif)

No *App*, para abrir um novo formulário, é necessário obter uma nova instância de um controlador de estudante, para isso pode-se criar um método estático em *StudentRepositoryController*:  

```java
public class StudentRepositoryController {
    // ...
    public static StudentRepositoryController createController(Database database) {
        StudentRepositoryView view = new StudentRepositoryView();
        StudentRepository model = new StudentRepository(database);
        StudentRepositoryController controller = new StudentRepositoryController(model, view);
        return controller;
    }
    // ...
}
```

Assim, o *App* pode obter a visão do controlador desejado e incluí-lo em uma aba do painel, quando a opção de menu *Repository > Student* for acionada:

```java
public class AppController
{
    public void init() {
        setStatus(AppModel.Status.DB_CLOSED);
        view.getMiStudent().addActionListener(e -> openStudentRepository());
        // ...
    }
    public void openStudentRepository() {
        StudentRepositoryController studentRepositoryController = StudentRepositoryController.createController(model.getDatabase());    
        view.getTpRepository().addTab("Student", studentRepositoryController.getView());
    }
}
```

Basicamente, o método *openStudentRepository* chama o método *createController* da classe *StudentRepositoryController*, passando a base de dados já previamente aberta do model. Depois adiciona a visão (view) deste controlador ao painel de abas (*JTabedPane*), atribuindo o nome "Student" a uma nova aba. Outros métodos de abertura (*fileOpen*), criação (*fileNew*) e fechamento (*fileClose*) de arquivo de base de dados, utilizam o diálogo de de arquivos (*JFileChooser*) e estão no código-fonte completo do controlador do *AppController*:

```java
public class AppController
{
    private AppModel model;
    private AppView view;
    
    public AppController(AppModel model, AppView view) {
        this.model = model;
        this.view = view;
        init();
    }
    
    public void setStatus(AppModel.Status status) {
        if (status == AppModel.Status.DB_CLOSED ) {
            view.getMRepository().setEnabled(false);
            view.getMiClose().setEnabled(false);
            view.getMiOpen().setEnabled(true);
            view.getMiNew().setEnabled(true);
        } else if (status == AppModel.Status.DB_OPEN ) {
            view.getMRepository().setEnabled(true);
            view.getMiOpen().setEnabled(false);
            view.getMiNew().setEnabled(false);
            view.getMiClose().setEnabled(true);        
        }
    }
    
    public void init() {
        setStatus(AppModel.Status.DB_CLOSED);
        view.getMiOpen().addActionListener(e -> fileOpen());
        view.getMiNew().addActionListener(e -> fileNew());
        view.getMiClose().addActionListener(e -> fileClose());  
        view.getMiStudent().addActionListener(e -> openStudentRepository());
    }
    
    private void openDatabase() {   
        model.setFile(view.getFileChooser().getSelectedFile());
        view.getLStatus().setText("File: "+model.getFile().getPath());
        Database database = new Database(model.getFile().getPath());
        model.setDatabase(database);
        setStatus(AppModel.Status.DB_OPEN);
    }
    
    public void fileOpen() {
        int returnVal = view.getFileChooser().showOpenDialog(view);
        if (returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
            openDatabase();
        } else {
            view.getLStatus().setText("Open file operation cancelled");
        }
    }
    
    public void fileNew() {
        int returnVal = view.getFileChooser().showSaveDialog(view);
        if (returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
            openDatabase();
        } else {
            view.getLStatus().setText("New file operation cancelled");
        }
    }
    
    public void fileClose() {
        model.getDatabase().close();
        setStatus(AppModel.Status.DB_CLOSED);
        view.getTpRepository().removeAll();
    }
    
    public void openStudentRepository() {
        StudentRepositoryController studentRepositoryController = StudentRepositoryController.createController(model.getDatabase());    
        view.getTpRepository().addTab("Student", studentRepositoryController.getView());
    }
    
    public static void main(String[] args) {
        AppModel model = new AppModel();
        AppView view = new AppView();
        AppController ac = new AppController(model, view);
        view.setVisible(true);
    }
}
```

## 7. Considerações finais

Neste tutorial, foi ensinado como criar uma aplicação CRUD no padrão MVC, quase completa, faltando somente a parte de *Update* e *Delete* que fica como exercício. Atualmente, ambos padrões são muito recorrentes na literatura e largamente utilizados no mercado. 

O padrão [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) não é arquitetural, mas sim operações comuns em sistemas que implementam algum tipo de persistência de dados. 

Novos padrões arquiteturais como o MVC tem evoluído e se estabelecendo aos poucos, como o [Model-view-viewmodel](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel), conhecido como MVVM e o [Model-view-presenter](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter). Didaticamente, o MVC é padrão fundamental para ser aprendido como passo inicial. 

### 7.1. Exercícios sugeridos

1. Implemente outra entidade que tenha relacionamento N:M com Student, por exemplo, criar a entide Class (turma), sendo que cada turma possui N estudantes e um Student pode participar de M turmas.
2. Implemente outra entidade que tenha relacionamento 1:N com estudante, por exemplo, Question (question). Por exemplo, caso o sistema tenha funcionalidade de acompanhamento de dúvidas, um estudante pode fazer N perguntas, sendo que cada pergunta tem um estudante como autor.
3. Utilize JFormattedTextField e MaskFormatter para [formatar a entrada de texto](https://www.tutorialspoint.com/swingexamples/creating_masked_textfield.htm).
4. Utilize interfaces ou classes abstratas para abstrair o tipo de banco de dados, facilitando a sua portabilidade. 
5. O SQLite é um banco de dados monousuário, ou seja, não há suporte a concorrência de acesso aos dados. Qual seria o impacto nas operações CRUD para dar suporte à concorrência? Dê um exemplo.
6. O que acontece com uma *view* quando um modelo que o mesmo acompanha sofrer uma alteração advinda de um evento não originado por essa mesma *view*, ou seja, por um controller não associado, por exemplo? Uma forma de atualizar a view, é fazer com que ela observe alterações de models, por meio de notificações e o padrão mais adequado é o padrão [Observer](https://en.wikipedia.org/wiki/Observer_pattern), pesquise como fazer isso e implemente essa funcionalidade. 
7. Por razões didáticas, o tutorial utilizou o ORMLite como tecnologia de mapeamento Objeto-Relacional (ORM). No Java há uma interface padrão chamada [Java Persistence API - JPA](https://pt.wikipedia.org/wiki/Java_Persistence_API) que possui vários ORMs como o [Hibernate](http://hibernate.org/) e o (OpenJPA)(http://openjpa.apache.org/), crie uma nova versão deste tutorial, utilizando uma dessas alternativas.
8. O diagrama de estados é uma representação visual do comportamento da transição de estados de um sistema. Um conceito abstrato de um sistema que possui estados e transições é a máquina de estado finito ou [Finite-State Machine ou FSM](https://en.wikipedia.org/wiki/Finite-state_machine), que pode possuir um implementação em java, por [exemplo](https://rosettacode.org/wiki/Finite_state_machine#Java). Implemente uma classse FSM que modela os estados e comportamentos do sistema e que a partir de um método calcula e fornece o próximo estado do sistema. Utilize essa classe para modelar e controlar o comportamento de um controlador, por exemplo, uma subclasse StudentRespositoryFSM derivado de uma classe FSM que modela o comportamento de um StudentRepositoryController.

Caso queira colaborar com o tutorial, oferecendo correções, versões alternativas ou código-fonte, não hesite em enviar um *Merge Request* para o [projeto](https://gitlab.com/marceloakira/tutorial/crud-mvc) ou [autor](https://gitlab.com/marceloakira).
