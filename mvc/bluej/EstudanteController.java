
/**
 * Escreva a descrição da classe EstudanteController aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class EstudanteController
{
    // variáveis de instância - substitua o exemplo abaixo pelo seu próprio
    private EstudanteView view;
    private Estudante model;

    /**
     * COnstrutor para objetos da classe EstudanteController
     */
    public EstudanteController(EstudanteView view, Estudante model)
    {
        this.model = model;
        this.view = view;
    }

    public void updateView() {
        view.printStudentDetails(model);
    }
}
