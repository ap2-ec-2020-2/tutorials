import javax.swing.JButton;
import java.util.List;
import javax.swing.JComponent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

/**
 * Escreva a descrição da classe EstudanteController aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class StudentRepositoryController
{
    // variáveis de instância - substitua o exemplo abaixo pelo seu próprio
    private StudentRepositoryView view;
    private StudentRepository model;
    
    /**
     * COnstrutor para objetos da classe EstudanteController
     */
    public StudentRepositoryController(StudentRepository model, StudentRepositoryView view)
    {
        this.model = model;
        this.view = view;
        init();
    }
    
    public void init() {
        model.setState(StudentRepository.State.INITIAL);
        view.getButtonFormPanel().getBCreate().addActionListener(e -> creating());
        view.getButtonFormPanel().getBSearch().addActionListener(e -> searching());
        view.getButtonFormPanel().getBOK().addActionListener(e -> commit());
        view.getButtonFormPanel().getBCancel().addActionListener(e -> cancel()); 
        view.getButtonFormPanel().getBFirst().addActionListener(e -> loading("FIRST"));
        view.getButtonFormPanel().getBPrevious().addActionListener(e -> loading("PREVIOUS"));          
        view.getButtonFormPanel().getBNext().addActionListener(e -> loading("NEXT"));
        view.getButtonFormPanel().getBLast().addActionListener(e -> loading("LAST"));
        updateView();
    }
    public void creating() {
      model.setState(StudentRepository.State.CREATING);       
      updateView();
    }
    public void searching() {
      model.setState(StudentRepository.State.SEARCHING);
      updateView();
    }
    public void loading(String action) {
      try {
          switch(action) {
              case "PREVIOUS":
                model.loadPrevious();
                if ( model.loadedFirst() )
                    model.setState(StudentRepository.State.LOADED_FIRST);
                else
                    model.setState(StudentRepository.State.LOADED_MIDDLE);
              break;
              case "NEXT":
                  model.loadNext();
                  if ( model.loadedLast() )
                    model.setState(StudentRepository.State.LOADED_LAST);
                  else
                    model.setState(StudentRepository.State.LOADED_MIDDLE);
              break;
              case "LAST":
                  model.loadLast();
                  model.setState(StudentRepository.State.LOADED_LAST);
              break;
              case "FIRST":
                model.loadFirst();
                model.setState(StudentRepository.State.LOADED_FIRST);
              break;
          }
      }
      catch (Exception e) {
          System.out.println("Error on loading. Details: "+e);
      }
      updateView();
    }
    private java.util.Date stringToDate( String string ) throws Exception {
        if ( string.equals("") )
            return null;
        Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
        Matcher matcher = dataPadrao.matcher(string);
        if (matcher.matches()) {
            int dia = Integer.parseInt(matcher.group(1));
            int mes = Integer.parseInt(matcher.group(2));
            int ano = Integer.parseInt(matcher.group(3));
            return (new GregorianCalendar(ano,mes,dia)).getTime();
        }
        else throw new Exception("Invalid data format");        
    }    

    public static StudentRepositoryController createController(Database database) {
        StudentRepositoryView view = new StudentRepositoryView();
        StudentRepository model = new StudentRepository(database);
        StudentRepositoryController controller = new StudentRepositoryController(model, view);
        return controller;
    }
    
    private int stringToInt( String string ) {
        return string.equals("") ? 0 : Integer.parseInt(string);
    }
    
    private Student loadFromView() throws Exception {
        Student studentLoaded = new Student();
        String strId = view.getStudentFormPanel().getTfId().getText();
        int id = stringToInt(strId);
        studentLoaded.setId(id);
        studentLoaded.setFullName(view.getStudentFormPanel().getTfFullname().getText());
        String strRegistration = view.getStudentFormPanel().getTfRegistration().getText();
        int registration = stringToInt(strRegistration);
        studentLoaded.setRegistration(registration);
        studentLoaded.setBirthday(stringToDate(view.getStudentFormPanel().getTfBirthday().getText()));
        return studentLoaded;
    }
    
    public void commit() {
        if ( model.getState() == StudentRepository.State.CREATING ) {
            try {
                Student studentLoaded = loadFromView();
                Student newStudent = model.create(studentLoaded);
                JOptionPane.showMessageDialog(view, "Student saved, with id: "+newStudent.getId());
                model.setState(StudentRepository.State.LOADED_ONE);
                updateView();
            } catch(Exception e) {
                JOptionPane.showMessageDialog(view,"Error: Student not saved. Details: "+e);
            } 
        } else if ( model.getState() == StudentRepository.State.SEARCHING ) {
              try {
                  Student studentLoaded = loadFromView();
                  if ( studentLoaded.getId() == 0 ) {
                      List<Student> studentsFound = model.loadAll();
                      if ( studentsFound.size() == 1 ) {
                          JOptionPane.showMessageDialog(view, "Student found: "+studentsFound.size()+" row");
                          model.setState(StudentRepository.State.LOADED_ONE );
                      } else if( studentsFound.size() > 1) { 
                          JOptionPane.showMessageDialog(view, "Students found: "+studentsFound.size()+" rows");
                          model.setState(StudentRepository.State.LOADED_FIRST );
                      } else
                          JOptionPane.showMessageDialog(view, "Students not found, repository is empty");
                  } else {
                      Student studentFound = model.loadFromId( studentLoaded.getId() );
                      if ( studentFound != null ) {
                          JOptionPane.showMessageDialog(view, "Student found with id: "+studentFound.getId());
                          model.setState(StudentRepository.State.LOADED_ONE);
                      }
                      else
                          JOptionPane.showMessageDialog(view, "Student not found");
                  }
                  updateView();
              } catch (Exception e) {
                  JOptionPane.showMessageDialog(view, "Error: Searching not performed. Details: "+e);
              }
        }     
    }
    public void cancel() {
        if ( model.getState() == StudentRepository.State.CREATING ) {
            try {
                int response = JOptionPane.showConfirmDialog(view, "Do you want to cancel and clear all fields?");
                if (response == JOptionPane.YES_OPTION ) {
                    model.setState(StudentRepository.State.INITIAL);
                    updateView();
                }
            } catch(Exception e) {
                JOptionPane.showMessageDialog(view,"Error: Student not saved. Detais: "+e);
            }
        } else if ( StudentRepository.LOADED.contains(model.getState()) || 
            model.getState() == StudentRepository.State.SEARCHING
        ) {
            model.setState(StudentRepository.State.INITIAL);
            updateView();
        }
    }
   
    private void enableComponents(List<JComponent> components) {
        for (JComponent component : components)
            component.setEnabled(true);
    }

    private void disableComponents(List<JComponent> components) {
        for (JComponent component : components)
            component.setEnabled(false);
    }
    
    private void clearFields(List<JComponent> components) {
        for (JComponent component: components)
            ((javax.swing.JTextField) component).setText("");
    }
    
    public void updateView() {
        if (model.getState() == StudentRepository.State.INITIAL ) {
            disableComponents(view.getButtonFormPanel().getNavigationButtons());
            disableComponents(view.getButtonFormPanel().getOkCancelButtons());
            disableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
            enableComponents(view.getButtonFormPanel().getSearchCreateButttons());
            clearFields(view.getStudentFormPanel().getAllFields());
            disableComponents(view.getStudentFormPanel().getAllFields());            
        } else if (model.getState() == StudentRepository.State.CREATING ) {
              disableComponents(view.getButtonFormPanel().getNavigationButtons());
              enableComponents(view.getButtonFormPanel().getOkCancelButtons());
              disableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
              disableComponents(view.getButtonFormPanel().getSearchCreateButttons());
              enableComponents(view.getStudentFormPanel().getAllFields());
              clearFields(view.getStudentFormPanel().getAllFields());                          
              view.getStudentFormPanel().getTfId().setEnabled(false);
        } else if (model.getState() == StudentRepository.State.SEARCHING ) {
              disableComponents(view.getButtonFormPanel().getNavigationButtons());
              enableComponents(view.getButtonFormPanel().getOkCancelButtons());
              disableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
              disableComponents(view.getButtonFormPanel().getSearchCreateButttons());
              disableComponents(view.getStudentFormPanel().getAllFields());
              view.getStudentFormPanel().getTfId().setEnabled(true);
              clearFields(view.getStudentFormPanel().getAllFields());            
        } else if (model.getState() == StudentRepository.State.DELETING ) {
            // implementação da visão no estado DELETING
        } else if (model.getState() == StudentRepository.State.UPDATING ) {
            // implementação da visão no estado UPDATING
        }
        else if ( StudentRepository.LOADED.contains(model.getState()) ) {
            try {
                loadFromModel();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(view, "Error: Repository not loaded. Details: "+e);
            }
            enableComponents(view.getButtonFormPanel().getNavigationButtons());
            disableComponents(view.getButtonFormPanel().getOkCancelButtons());
            view.getButtonFormPanel().getBCancel().setEnabled(true);
            enableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
            disableComponents(view.getButtonFormPanel().getSearchCreateButttons());
            disableComponents(view.getStudentFormPanel().getAllFields());
            if ( model.getState() == StudentRepository.State.LOADED_ONE ) {
                disableComponents(view.getButtonFormPanel().getNavigationButtons());
            } else if ( model.getState() == StudentRepository.State.LOADED_FIRST ) {
                view.getButtonFormPanel().getBFirst().setEnabled(false);
                view.getButtonFormPanel().getBPrevious().setEnabled(false);
            } else if ( model.getState() == StudentRepository.State.LOADED_LAST ) {
                view.getButtonFormPanel().getBLast().setEnabled(false);
                view.getButtonFormPanel().getBNext().setEnabled(false);
            }
        }   
    }
    private String dateToString( java.util.Date date ) {
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }    
    public void loadFromModel() throws Exception {
        Student studentLoaded = model.getLoadedStudent();
        if (studentLoaded == null)
            throw new Exception("Student not loaded");
        view.getStudentFormPanel().getTfId().setText(String.valueOf(studentLoaded.getId()));
        view.getStudentFormPanel().getTfFullname().setText(studentLoaded.getFullName());
        view.getStudentFormPanel().getTfRegistration().setText(String.valueOf(studentLoaded.getRegistration()));
        view.getStudentFormPanel().getTfBirthday().setText(dateToString(studentLoaded.getBirthday()));
        List<Student> studentsLoaded = model.getLoadedStudents();
        if (studentsLoaded == null)
            throw new Exception("Repository is not loaded");
        int size = studentsLoaded.size();
        int id = studentsLoaded.indexOf(studentLoaded);
        view.getButtonFormPanel().getLRegisters().setText("row "+(id+1)+" of "+size+" rows");
    }
    /**GET Method Propertie view*/
    public StudentRepositoryView getView(){
        return this.view;
    }//end method getView

    /**SET Method Propertie view*/
    public void setView(StudentRepositoryView view){
        this.view = view;
    }//end method setView

    /**GET Method Propertie model*/
    public StudentRepository getModel(){
        return this.model;
    }//end method getModel

    /**SET Method Propertie model*/
    public void setModel(StudentRepository model){
        this.model = model;
    }//end method setModel

    public static void main(String[] args) {
        StudentRepository model = new StudentRepository(new Database(args[0]));
        StudentRepositoryView view = new StudentRepositoryView();
        StudentRepositoryController studentRepositoryController = new StudentRepositoryController(model, view);
        javax.swing.JFrame jFrame = new javax.swing.JFrame();
        jFrame.setSize(900,600);
        jFrame.setLayout(new java.awt.BorderLayout());
        jFrame.add(view, java.awt.BorderLayout.CENTER);
        jFrame.setVisible(true);
    }
    //End GetterSetterExtension Source Code
//!
}
