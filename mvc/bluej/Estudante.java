
/**
 * Escreva a descrição da classe Estudante aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import java.util.GregorianCalendar;
import java.time.format.DateTimeFormatter;

public class Estudante
{   
    private String nomeCompleto;
    public int matricula;
    public GregorianCalendar dataDeNascimento;    

    public void setNomeCompleto(String nomeCompleto) 
    {
        this.nomeCompleto = nomeCompleto;
    }
    
    public String getNomeCompleto() {
        return this.nomeCompleto;
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie matricula*/
    public int getMatricula(){
        return this.matricula;
    }//end method getMatricula

    /**SET Method Propertie matricula*/
    public void setMatricula(int matricula){
        this.matricula = matricula;
    }//end method setMatricula

    /**GET Method Propertie dataDeNascimento*/
    public GregorianCalendar getDataDeNascimento(){
        return this.dataDeNascimento;
    }//end method getDataDeNascimento
    
    public String printDataDeNascimento() {
        // String ano = String.valueOf(dataDeNascimento.YEAR);
        // String mes = String.valueOf(dataDeNascimento.MONTH);
        // String dia = String.valueOf(dataDeNascimento.DAY_OF_MONTH);
        // return dia+"/"+mes+"/"+ano;
        return dataDeNascimento.toZonedDateTime().format(DateTimeFormatter.ofPattern("dd/MM/uuuu"));
    }    
    /**SET Method Propertie dataDeNascimento*/
    public void setDataDeNascimento(GregorianCalendar dataDeNascimento){
        this.dataDeNascimento = dataDeNascimento;
    }//end method setDataDeNascimento

    //End GetterSetterExtension Source Code
//!
}
