import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.EnumSet;

public class StudentRepository
{
    private static Database database;
    private static Dao<Student, Integer> dao;
    public enum State { INITIAL, SEARCHING, CREATING, DELETING, UPDATING, 
        LOADED_ONE, LOADED_FIRST, LOADED_LAST, LOADED_MIDDLE };
    public static Set<State> LOADED = EnumSet.of(State.LOADED_ONE, State.LOADED_FIRST,
                               State.LOADED_LAST, State.LOADED_MIDDLE);
    private State state = State.INITIAL;
    private List<Student> loadedStudents;
    private Student loadedStudent; 

    public void loadFirst() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        setLoadedStudent(loadedStudents.get(0));
    }    
    
    public void loadPrevious() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        int index = loadedStudents.indexOf(loadedStudent);
        if ( index == 0 )
            throw new Exception("Could not loaded previous student, current index is 0");
        setLoadedStudent(loadedStudents.get(index-1));
    }

    public void loadNext() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        int index = loadedStudents.indexOf(loadedStudent);
        if ( index == loadedStudents.size()-1 )
            throw new Exception("Could not loaded next student, current index is the last");
        setLoadedStudent(loadedStudents.get(index+1));
    }
    
    public void loadLast() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        int last = loadedStudents.size()-1;
        setLoadedStudent(loadedStudents.get(last));
    }        
    
    public boolean loadedLast() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        if ( loadedStudents.indexOf(loadedStudent)+1 == loadedStudents.size() )
            return true;
        else
            return false;
    }
    
    public boolean loadedFirst() throws Exception {
        if ( loadedStudent == null || loadedStudents == null || loadedStudents.size() == 0)
            throw new Exception("Students not loaded");
        if ( loadedStudents.indexOf(loadedStudent) == 0 )
            return true;
        else
            return false;
    }
    
    public StudentRepository(Database database) {
        StudentRepository.setDatabase(database);
        loadedStudents = new ArrayList<Student>();
    }
    
    public static void setDatabase(Database database) {
        StudentRepository.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Student.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Student.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    }
    
    public Student create(Student student) {
        int nrows = 0;
        try {
            nrows = dao.create(student);
            if ( nrows == 0 )
                throw new SQLException("Error: object not saved");
            this.loadedStudent = student;
            loadedStudents.add(student);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return student;
    }    

    public void update(Student student) {
      // TODO
    }

    public void delete(Student student) {
      // TODO
    }
    
    public Student loadFromId(int id) {
        try {
            this.loadedStudent = dao.queryForId(id);
            if (this.loadedStudent != null)
                this.loadedStudents.add(this.loadedStudent);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedStudent;
    }    
    
    public List<Student> loadAll() {
        try {
            this.loadedStudents =  dao.queryForAll();
            if (this.loadedStudents.size() != 0)
                this.loadedStudent = this.loadedStudents.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedStudents;
    }

    // getters and setters ommited...        

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie database*/
    public Database getDatabase(){
        return this.database;
    }//end method getDatabase

    /**GET Method Propertie dao*/
    public com.j256.ormlite.dao.Dao<Student, java.lang.Integer> getDao(){
        return this.dao;
    }//end method getDao

    /**SET Method Propertie dao*/
    public void setDao(com.j256.ormlite.dao.Dao<Student, java.lang.Integer> dao){
        this.dao = dao;
    }//end method setDao

    /**GET Method Propertie status*/
    public State getState(){
        return this.state;
    }//end method getState

    /**SET Method Propertie status*/
    public void setState(State state){
        this.state = state;
    }//end method setStatus

    /**GET Method Propertie loadedStudents*/
    public java.util.List<Student> getLoadedStudents(){
        return this.loadedStudents;
    }//end method getLoadedStudents

    /**SET Method Propertie loadedStudents*/
    public void setLoadedStudents(java.util.List<Student> loadedStudents){
        this.loadedStudents = loadedStudents;
    }//end method setLoadedStudents

    /**GET Method Propertie loadedStudent*/
    public Student getLoadedStudent(){
        return this.loadedStudent;
    }//end method getLoadedStudent

    /**SET Method Propertie loadedStudent*/
    public void setLoadedStudent(Student loadedStudent){
        this.loadedStudent = loadedStudent;
    }//end method setLoadedStudent

    //End GetterSetterExtension Source Code
//!
}