
/**
 * Escreva a descrição da classe AppController aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class AppController
{
    private AppModel model;
    private AppView view;
    
    public AppController(AppModel model, AppView view) {
        this.model = model;
        this.view = view;
        init();
    }
    
    public void setStatus(AppModel.Status status) {
        if (status == AppModel.Status.DB_CLOSED ) {
            view.getMRepository().setEnabled(false);
            view.getMiClose().setEnabled(false);
            view.getMiOpen().setEnabled(true);
            view.getMiNew().setEnabled(true);
        } else if (status == AppModel.Status.DB_OPEN ) {
            view.getMRepository().setEnabled(true);
            view.getMiOpen().setEnabled(false);
            view.getMiNew().setEnabled(false);
            view.getMiClose().setEnabled(true);        
        }
    }
    
    public void init() {
        setStatus(AppModel.Status.DB_CLOSED);
        view.getMiOpen().addActionListener(e -> fileOpen());
        view.getMiNew().addActionListener(e -> fileNew());
        view.getMiClose().addActionListener(e -> fileClose());  
        view.getMiStudent().addActionListener(e -> openStudentRepository());
    }
    
    private void openDatabase() {   
        model.setFile(view.getFileChooser().getSelectedFile());
        view.getLStatus().setText("File: "+model.getFile().getPath());
        Database database = new Database(model.getFile().getPath());
        model.setDatabase(database);
        setStatus(AppModel.Status.DB_OPEN);
    }
    
    public void fileOpen() {
        int returnVal = view.getFileChooser().showOpenDialog(view);
        if (returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
            openDatabase();
        } else {
            view.getLStatus().setText("Open file operation cancelled");
        }
    }
    
    public void fileNew() {
        int returnVal = view.getFileChooser().showSaveDialog(view);
        if (returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
            openDatabase();
        } else {
            view.getLStatus().setText("New file operation cancelled");
        }
    }
    
    public void fileClose() {
        model.getDatabase().close();
        setStatus(AppModel.Status.DB_CLOSED);
        view.getTpRepository().removeAll();
    }
    
    public void openStudentRepository() {
        StudentRepositoryController studentRepositoryController = StudentRepositoryController.createController(model.getDatabase());    
        view.getTpRepository().addTab("Student", studentRepositoryController.getView());
    }
    
    public static void main(String[] args) {
        AppModel model = new AppModel();
        AppView view = new AppView();
        AppController ac = new AppController(model, view);
        view.setVisible(true);
    }
}
