# Tutorial básico de Git

## Criar um projeto

![criar projeto](https://gitlab.com/ap2-ec-2020-2/tutorials/-/raw/master/git/criar-projeto.gif)


## Clonar, confirmar e enviar alterações (clone-commit-push)

Considerando-se que o desenvolvedor tenha permissão para enviar alterações diretamente para a branch master:

![clone, branch and push](https://gitlab.com/ap2-ec-2020-2/tutorials/-/raw/master/git/clone-commit-push.gif)

## Clonar, criar uma branch e enviar alterações (clone-branch-push)

Considerando-se que o desenvolvedor não tenha permissão para enviar alterações diretamente para a branch master e tenha que criar uma nova branch no repositório:

![clone, branch and push](https://gitlab.com/ap2-ec-2020-2/tutorials/-/raw/master/git/clone-branch-push.gif)

## Submeter e revisar uma solicitação de mesclagem (merge-request)

Considerando-se que o desenvolvedor solicita uma mesclagem de código de uma branch de origem (teste) para uma branch de destino (master):

![merge request](https://gitlab.com/ap2-ec-2020-2/tutorials/-/raw/master/git/merge-request.gif)


